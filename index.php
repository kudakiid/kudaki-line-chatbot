<?php
require __DIR__ . '/vendor/autoload.php';

use \LINE\LINEBot;
use \LINE\LINEBot\HTTPClient\CurlHTTPClient;
use \LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use \LINE\LINEBot\SignatureValidator as SignatureValidator;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Http\Response;

// set false for production
$pass_signature = true;

// set LINE channel_access_token and channel_secret
$channel_access_token = "M3orB/WluW4fmOdk6jISNlTanHb1WifKGDXNVeg3asg8Z9t7p+ouBK3uDFXqfKA+pIft/Udeerl6CT68f8PxFfomhzLG54FDHie2G89kCw+Bju9ACEQUAg2pLYsPZHYw6c8JFnd+eQaHbktG/tl8kwdB04t89/1O/w1cDnyilFU=";
$channel_secret = "f3f843efe71660ebd87f39e50515b51d";

// inisiasi objek bot
$httpClient = new CurlHTTPClient($channel_access_token);
$bot = new LINEBot($httpClient, ['channelSecret' => $channel_secret]);

$configs = [
    'settings' => ['displayErrorDetails' => true],
];
$app = new Slim\App($configs);

// buat route untuk url homepage
$app->get('/', function ($req, $res) {
    $flexTemplate = file_get_contents("event-line.json");
    var_dump(json_decode($flexTemplate)->data);
});

$app->get('/pushmessage', function ($req, $res) use ($bot) {
    // send push message to user
    $userId = 'U4a23460ed672d1a409d14a3eaee31b44';
    $textMessageBuilder = new TextMessageBuilder('Halo, ini pesan push');
    $result = $bot->pushMessage($userId, $textMessageBuilder);

    return $res->withJson($result->getJSONDecodedBody(), $result->getHTTPStatus());
});

$app->get('/profile', function ($req, Response $res) use ($bot) {
    // get user profile
    $userId = 'U4a23460ed672d1a409d14a3eaee31b44';
    $result = $bot->getProfile($userId);

    return $res->withJson($result->getJSONDecodedBody()['displayName'], $result->getHTTPStatus());
});

$app->get('/content/{messageId}', function ($req, $res) use ($bot) {
    // get message content
    $route = $req->getAttribute('route');
    $messageId = $route->getArgument('messageId');
    $result = $bot->getMessageContent($messageId);

    // set response
    $res->write($result->getRawBody());

    return $res->withHeader('Content-Type', $result->getHeader('Content-Type'));
});

// buat route untuk webhook
$app->post('/webhook', function (Request $request, Response $response) use ($bot, $pass_signature, $httpClient) {
    // get request body and line signature header
    $body = file_get_contents('php://input');
    $signature = isset($_SERVER['HTTP_X_LINE_SIGNATURE']) ? $_SERVER['HTTP_X_LINE_SIGNATURE'] : '';

    // log body and signature
    file_put_contents('php://stderr', 'Body: ' . $body);

    if ($pass_signature === false) {
        // is LINE_SIGNATURE exists in request header?
        if (empty($signature)) {
            return $response->withStatus(400, 'Signature not set');
        }

        // is this request comes from LINE?
        if (!SignatureValidator::validateSignature($body, $channel_secret, $signature)) {
            return $response->withStatus(400, 'Invalid signature');
        }
    }

    $data = json_decode($body, true);
    if (is_array($data['events'])) {
        foreach ($data['events'] as $event) {
            if ($event['type'] == 'message') {
                if ($event['message']['type'] == 'text') {
                    switch (strtolower($event['message']['text'])) {
                        case 'halo':
                            $result = $bot->getProfile($event['source']['userId']);
                            $result = $bot->replyText($event['replyToken'],
                                "Halo juga, Kak ".$result->getJSONDecodedBody()['displayName']);
                            return $response->withJson($result->getHTTPStatus());
                        case 'browse event':
                            // get from endpoint
                            $flexTemplate = file_get_contents("event-line.json");

                            $result = $httpClient->post(LINEBot::DEFAULT_ENDPOINT_BASE . '/v2/bot/message/reply', [
                                'replyToken' => $event['replyToken'],
                                'messages' => [
                                    [
                                        'type' => 'flex',
                                        'altText' => 'Flex Message',
                                        'contents' => json_decode($flexTemplate)
                                    ]
                                ],
                            ]);
                            return $response->withJson($result->getHTTPStatus());
                    }
                }
            }
        }
    }
    return http_response_code(200);
});

$app->run();